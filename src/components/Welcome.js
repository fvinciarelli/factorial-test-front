import 'bootstrap/dist/css/bootstrap.min.css';
import React, {Component} from 'react'
import { Link } from 'react-router-dom';
import { Container, Image, Row, Col } from 'react-bootstrap';

class WelcomePage extends Component {

    render() {
        return (
            <Container style={{"marginTop":"40px"}}>
                <Row>
                    <Col className="text-center">
                        <h4>Vinciarelli Test's</h4>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center">
                        <Image roundedCircle height="400px" width="400px" src="https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/hc_1440x810/public/media/image/2020/09/simpson-homer-2083091.jpg?h=785f7ce4&itok=90UR4FbI"></Image>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center">
                        <Link to='/MetricsForm'>Let's Add some Metric!</Link>
                    </Col>
                    <Col className="text-center">
                        <Link to='/MetricsGraph'>View Metric's graph</Link>
                    </Col>
                </Row>
            </Container>
        )
    }

}

export default WelcomePage;