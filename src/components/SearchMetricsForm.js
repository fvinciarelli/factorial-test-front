import React, { useState, useEffect } from 'react';
import { Container, Row, Form, Col, FormGroup, FormControl, Button } from 'react-bootstrap';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import moment from 'moment';
import 'bootstrap-daterangepicker/daterangepicker.css';
import axios from 'axios';

const SearchMetricsForm = ({handleSearhFields}) => {

    let defaultStartDate = moment().format("MM/DD/YYYY");

    const [formData, setFormData] = useState({metric: "all", fromDate: defaultStartDate , toDate: defaultStartDate, avgWindow: '900'});
    const [lstMetrics, setLstMetrics] = useState([]);
    const [fireSearch, setFireSearch] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                // get the data from the api
                let response = await axios.get('http://localhost:3000/api/v1/metrics');
                let adjustedData = response.data.map (r => {return {value: r.id, label: r.name.toUpperCase()}});
                setLstMetrics(adjustedData);
            } catch(err) {
                console.log(err);
            }
        }

        fetchData();
    }, []);

    useEffect(() => {
        if(fireSearch) {
            handleSearhFields(formData);
            setFireSearch(false);
        }
    }, [fireSearch]);

    const handleChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value}); 
    }

    const handleCallback = (start, end, label) => {
        setFormData({...formData,'fromDate': start.format('YYYY-MM-DD'), 'toDate': end.format('YYYY-MM-DD') });
    }

    const handleBtnFindClick = (e) => {
        e.preventDefault();
        setFireSearch(true); 
    }

    return (
        <Container>
            <Form>
                <Row>
                    <Col>
                        <FormGroup>
                            <Form.Label>Metrics:</Form.Label>
                            <Form.Control 
                                as="select" 
                                name="metric"
                                onChange={handleChange}
                            >
                                <option value="all" defaultValue>All</option>
                                {lstMetrics.map((item, index) => (
                                    <option key={index} value={item.id}>
                                    {item.label}
                                    </option>
                                ))}
                            </Form.Control>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Form.Label>From/To Date:</Form.Label>
                            <DateRangePicker
                                initialSettings={{ startDate: { defaultStartDate }, endDate: '12/01/2022' }}
                                onCallback={handleCallback} 
                            >
                            <FormControl type="text"></FormControl>
                            </DateRangePicker>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Form.Label>Average:</Form.Label>
                            <Form.Control 
                                as="select" 
                                name="avgWindow"
                                onChange={handleChange}
                            >
                                <option value="86400">By Day</option>
                                <option value="3600">By Hour</option>
                                <option value="1800">By 30 minutes</option>
                                <option value="900" default>By 15 minutes</option>
                                <option value="60">By 01 minute</option>
                            </Form.Control>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Button style={{marginTop:32}} type="primary" onClick={handleBtnFindClick}>Find</Button>
                        </FormGroup>
                    </Col>
                </Row>
            </Form>
        </Container>
    );
};

export default SearchMetricsForm;