import React, { useEffect, useState } from 'react';
import TimeSeriesChart from './TimeSeriesChart';
import SearchMetricsForm from './SearchMetricsForm';
import axios from 'axios';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Navigate } from "react-router-dom";
//import MetricTimeline from './MetricTimeline';

const MetricsGraph = () => {

    let [data, setData] = useState([]);
    const [searchData, setSearchData] = useState({});
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        
        const fetchData = async () => {
            let graphSerie = [];
            try {
                // get the data from the api
                let response = await axios.get('http://localhost:3000/api/v1/graph_data',  {
                                                params: {
                                                    metric: ('metric' in searchData) ? searchData.metric : 'all',
                                                    fromDate: ('fromDate' in searchData) ? searchData.fromDate : '2022-01-01',
                                                    toDate: ('toDate' in searchData) ? searchData.toDate : '2022-12-01',
                                                    avgWindow: ('avgWindow' in searchData) ? searchData.avgWindow : '900',
                                                }
                                            });
                
                Object.entries(response.data).forEach(entry => {
                    let [key, value] = entry;
                    graphSerie.push({
                        serie: key,
                        data: value.map(r => {return { value: r.value, time: toTimestamp(r.timestamp) } })
                    });
                  });
                setData(graphSerie);
            } catch(err) {
                console.log(err);
            }
        }

        fetchData();

    }, [searchData]);

    const toTimestamp = (strDate) => {  
        const dt = Date.parse(strDate);  
        return dt;  
    } 

    const handleSearch = (parametros) => {
        setSearchData(parametros)
    }
    
    const getBack = (e) => {
        e.preventDefault();    
        setRedirect(true);
    }
    return (
        <div>
            <Container>
                {redirect && (<Navigate to='/' replace={true} />)}
                <Row className="text-center">
                    <Col>
                        <h4>Metrics Graph</h4>
                    </Col>    
                </Row>
                <SearchMetricsForm handleSearhFields={handleSearch} />
                <Row>
                    <Col>
                        <TimeSeriesChart chartData={data} />
                    </Col>    
                </Row>
                <Row className="text-center">
                    <Col>
                        <Button variant="primary" onClick={getBack} style={{"marginRight":"20px"}}>Back</Button>
                    </Col>
                </Row>
            </Container>
            
        </div>
    )
    
}

export default MetricsGraph;