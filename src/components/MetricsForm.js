import React, {Component} from "react";
import { Container, Row, Col, Form, FormGroup, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import axios from 'axios';

class MetricsForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            metric_name: '',
            metric_value: '',
            redirect : false
        }
        
        this.handleChange = this.handleChange.bind(this);
    }
    
    
    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    saveMetric = async (e) => {
        e.preventDefault();

        try {
            let response = await axios.post('http://localhost:3000/api/v1/metrics', 
                                            {
                                              name: this.state.metric_name,
                                              value: this.state.metric_value
                                            });
            this.setState({ metric_name: '', metric_value: ''});
            alert("Weee! Metric has been added correctly!");
        } catch (err) {
            alert (":-( Something went wrong!" + err);
        }

    }

    getBack = (e) => {
        e.preventDefault();    
        this.setState({redirect: true});
    }

    render() {
                let {redirect} = this.state;
                return (
                    <Container style={{"marginTop":"40px"}}>
                        {redirect && (<Navigate to='/' replace={true} />)}
                        <Row>
                            <Col className="text-center">
                                <h4>New Metric</h4>
                            </Col>
                        </Row>
                        <Form>
                        <Row>
                            <Col>
                                <FormGroup controlId="txtMetricName">
                                    <Form.Label>Metric Name:</Form.Label>
                                    <Form.Control name="metric_name" type="text" placeholder = 'Metric Name' onChange={this.handleChange} value={this.state.metric_name} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup controlId="txtMetricValue">
                                    <Form.Label>Metric Value:</Form.Label>
                                    <Form.Control name="metric_value" type="text" placeholder = 'Metric value' onChange={this.handleChange} value={this.state.metric_value} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="text-center" style={{"padding":"10px"}}>
                                <Button variant="secondary" onClick={this.getBack} style={{"marginRight":"20px"}}>Back</Button>
                                <Button variant="primary" onClick={this.saveMetric}>Save</Button>
                            </Col>
                        </Row>
                        </Form>
                    </Container>
                )
    }
}

export default MetricsForm;