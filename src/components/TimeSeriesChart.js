import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

import {
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Scatter,
  ScatterChart,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'

const TimeSeriesChart = ({chartData}) => {
  console.log(chartData);

  return (
    <ResponsiveContainer width = '90%' height = {400} >
      <ScatterChart 
            margin={{
               top: 20,
               right: 20,
               bottom: 20,
               left: 20
            }}
      >
        <Legend verticalAlign="top" height={36}/>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          dataKey = 'time'
          domain = {['auto', 'auto']}
          name = 'Time'
          tickFormatter = {(unixTime) => moment(unixTime).format('YYYY-MM-DD HH:mm')}
          type = 'number'
        />
        <YAxis dataKey = 'value' name = 'Value' />
        {
          chartData.map(function(s) {
            return (
            <Scatter
              key = {s.data}
              data = {s.data}
              line = {{ stroke: "#" + Math.floor(Math.random()*16777215).toString(16), strokeWidth: 2 }}
              lineJointType = 'monotoneX'
              lineType = 'joint'
              name = {s.serie}
              />)
          })
        }
        <Tooltip />   
      </ScatterChart>
    </ResponsiveContainer>
  )
};

export default TimeSeriesChart;