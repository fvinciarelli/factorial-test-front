import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import MetricsForm from './components/MetricsForm';
import MetricsGraph from './components/MetricsGraph';

import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App/>} />
      <Route path="/MetricsForm" element={<MetricsForm/>} />
      <Route path="/MetricsGraph" element={<MetricsGraph/>} />
    </Routes>
  </BrowserRouter>
);

