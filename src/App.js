import WelcomePage from './components/Welcome';

function App() {
  return (
    <div>
      <WelcomePage />
    </div>
  )
}

export default App;
